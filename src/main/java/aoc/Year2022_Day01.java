package aoc;

import java.util.ArrayList;
import java.util.Arrays;

// Put the return type of the generator in Base<>
public class Year2022_Day01 extends Base<ArrayList<ArrayList<Integer>>> {

    // The generator's role is to turn teh input file into something useable by part 1 and part 2.
    void generator() {
        // this must have the same type as the return type of this method
        this.processed = new ArrayList<>();
        // read it line by line
        ArrayList<Integer> elf = new ArrayList<>();
        for(String line: this.input.split("\n")){
            if(line.equals("")){
                this.processed.add(elf);
                elf = new ArrayList<>();
            } else {
                elf.add(Integer.parseInt(line, 10));
            }
        }

        this.processed.add(elf);
    }

    void part1() {

        int counter = 0;
        for (ArrayList<Integer> elf : this.processed) {
            int calories = 0;

            for(int item: elf){
                calories += item;
            }

            if (calories > counter){
                counter = calories;
            }
        }

        // print out teh result
        System.out.printf("Part 1: %d %n", counter);
    }

    void part2() {
        int[] max = {0,0,0};

        for (ArrayList<Integer> elf : this.processed) {
            int calories = 0;

            for(int item: elf){
                calories += item;
            }

            if (calories > max[0]){
                max[0] = calories;
                Arrays.sort(max);
            }
        }

        int counter = max[0] + max[1] + max[2];

        // print out teh result
        System.out.printf("Part 2: %d %n", counter);
    }
}
